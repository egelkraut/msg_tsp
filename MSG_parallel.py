#!/usr/bin/env python3
# coding: utf-8


import pandas as pd
from geopy import distance
import numpy as np
import itertools
import multiprocessing
from functools import partial
from joblib import Parallel, delayed


def main():
    csv = pd.read_csv("msg_standorte_deutschland.csv")
    #csv = csv.head(19) #This is a test option to reduce operation time


    locations = [distance.lonlat(a, b) for a, b in zip(csv['Längengrad'], csv["Breitengrad"])]


    xy, yx = np.meshgrid(locations, locations)
    vec_dist_func = np.vectorize(distance.distance)
    vec_to_km_func = np.vectorize(lambda location: location.km)
    C = vec_to_km_func(vec_dist_func(xy, yx))


    # Use Held-Karp algorithm:
    # 
    # g(a, set_b) with k is len(set_b)
    # 
    # https://en.wikipedia.org/wiki/Held%E2%80%93Karp_algorithm#Example[4]

    r = Results(C)

    g, p = r.wholeCalc()
    print(g)
    print(p)


    #Test path
    dist = 0
    previous_point = 0
    for x in p:
        dist += C[x, previous_point]
        previous_point = x
    if (dist - g < 0.001):
        print("\nSanity check successful, trip is " + str(round(dist, 3)) + "km long. (Geodesic distance)")
    else:
        print("Sanity check failed")



    print("Following route is proposed:\n")
    print(csv["Ort"].loc[p].to_list())





class Results:
    def __init__(self, C):
        self.C = C
        if (C.shape[0] == C.shape[1]):
            self.n = C.shape[0]
        else:
            raise ValueError("C is not quadratic!")
        if (self.n < 2):
            raise ValueError("C is to small!")
        self.resultsToK = 0 #Tells to which order the results are already stored
        
        self.g = dict()  #Each k value will get own nested dictionary
        self.p = dict()
        self.savedKs = {0}
        
        for k in range(0, self.n):
            self.g[k] = dict()
            self.p[k] = dict()
        
        
            
        #Do k=0 calculations:
        
        for node in range(0, self.n):
            self.g[0][(node, frozenset({}))] = C[node, 0]
            self.p[0][(node, frozenset({}))] = [0]
        

    @staticmethod   
    def __getElement(dictionary, node, nodeSet):
        nodeSet = frozenset(nodeSet)
        return dictionary[len(nodeSet)][(node, nodeSet)]
    
    @staticmethod
    def __setElement(dictionary, node, nodeSet, value):
        nodeSet = frozenset(nodeSet)
        dictionary[len(nodeSet)][(node, nodeSet)] = value
        
    def delK(self, k):  #Deletes saved iterim results from orders which are no longer neccessary.
        self.g[k].clear()
        self.p[k].clear()
        self.savedKs -= {k}
        
        
        
        
    def getG(self, node, nodeSet):
        try:
            output = Results.__getElement(self.g, node, nodeSet)
        except KeyError:
            raise ValueError("Didn't calculate the value of g(" + str(node) + ", " + str(nodeSet) + ") yet.")
            
        return output
        
    def setG(self, node, nodeSet, value):
        Results.__setElement(self.g, node, nodeSet, value)
        
    def getP(self, node, nodeSet):
        try:
            output = Results.__getElement(self.p, node, nodeSet)
        except KeyError:
            raise ValueError("Didn't calculate the value of p(" + str(node) + ", " + str(nodeSet) + ") yet.")
            
        return output
        
    def setP(self, node, nodeSet, value):
        oldpath = self.getP(value, nodeSet-{value})
        oldpath = list(oldpath)
        oldpath.append(value)  #Keep previous nodes, in order to be able to reconstruct path at the end.
        Results.__setElement(self.p, node, nodeSet, oldpath)
        
    def calcToK(self, kGoal):  #Do the calculations to order kGoal
        if (kGoal in self.savedKs):
            return
        
        if (kGoal >= self.n-1):
            self.wholeCalc()
            
        self.calcToK(kGoal-1)

        allNodes = range(1, self.n)
        
        
        partial_batch_calcToK = partial(self.batch_calcToK, kGoal=kGoal)
        Parallel(n_jobs=multiprocessing.cpu_count(), require='sharedmem')(delayed(partial_batch_calcToK)(node) for node in allNodes)


        self.delK(kGoal-1)
        self.savedKs.add(kGoal)
        print("Order k = " + str(kGoal) + " done!")

    def batch_calcToK(self, node, kGoal):
        nodeSet = set(range(1, self.n)) - {node} #Ignore Node 0 and node
        
        
        for nodeTupel in itertools.combinations(nodeSet, kGoal):
            nodeTupel = set(nodeTupel)

            g, p = self.singleCalc(node, nodeTupel)
            self.setG(node, nodeTupel, g)
            self.setP(node, nodeTupel, p)

        return self.g[kGoal], self.p[kGoal]


        
    def singleCalc(self, node, nodeSet, saveResult=False):  #Calculates g and p for one single input.
        gValList = []
        pValList = []
        
        for thisNode in nodeSet:
            thisNodeSet= nodeSet.copy()
            thisNodeSet.remove(thisNode)
            
            gValList.append(self.C[node, thisNode] + self.getG(thisNode, thisNodeSet))
            pValList.append(thisNode)
        
        i = gValList.index(min(gValList))
        g, p = gValList[i], pValList[i]

        if saveResult:
            self.setG(node, nodeSet, g)
            self.setP(node, nodeSet, p)
            print("g{" + str(node) + ", " + str(nodeSet) + "}.")

        return g, p
    
    def wholeCalc(self): #Does the whole calculations.
        AllNodeSet = set(range(1, self.n)) #Ignore Node 0
        if(self.n-1 not in self.savedKs):
            print("\nWill calculate order 1 to " + str(self.n-1) + ". The orders in the middle of this interval are the slowest.\n")
            self.calcToK(self.n-2)
            
            g, p = self.singleCalc(0, AllNodeSet)
            self.setG(0, AllNodeSet, g)
            self.setP(0, AllNodeSet, p)
            self.delK(self.n-2)
            self.savedKs.add(self.n-1)
            print("Order k = " + str(self.n-1) + " done!\n")
        return self.getG(0, AllNodeSet), self.getP(0, AllNodeSet)+[0]


if __name__ == '__main__':
    main()