## Technical details

This project is written in python and uses several packages. You can find the requirements in the requirements.txt. 
Additionally, there's a parallel version. It needs also the joblib package.

The packages were installes using pip3. Following packages (and their dependencies) should include everything you need:
 - pandas
 - geopy
 - (joblib)

Therefore it should be enough (Linux) to execute `pip3 install pandas geopy` (`pip3 install pandas geopy joblib`).
The Jupyter Notebook and the python script contain the same algorithm. The sole difference is, that the notebook contains more output. The parallel version is only available as python script. 
All versions must be started in the same folder as the `msg_standorte_deutschland.csv` file. 
 
 
 ## Algorithm
 
 This project uses the Held-Karp algorithm, which gives an exact solution. 
 Consider following web page: https://en.wikipedia.org/wiki/Held%E2%80%93Karp_algorithm#Example%5B4%5D
 
 Disadvantage: It needs around 9min to finish and a PC with at least 8GB main memory. But if you will travel 2337km, this should be accaptable. 
The parallel version is not that much faster, as the limiting factor seems to be the read/write operations. 

As even small errors (2% of 2337km are roughly 50km) are noticable, an exact algrorithm was preferred. 
This algorithm was choosen, because its a comparable fast exact one. 
 
 ## Result:
 
 Trip Length: 2337km
 Trip:
1.  Ismaning
2.  Ingolstadt
2.  Nürnberg
3.  Leinfelden-Echterdingen
4.  St. Georgen
5.  Bretten
6.  Walldorf
7.  Eschborn
8.  Hürth
9.  Düsseldorf
10.  Essen
11.  Münster
12.  Lingen
13.  Schortens
14.  Hamburg
15.  Hannover
16.  Braunschweig
17.  Berlin
18.  Görlitz
19.  Chemnitz
20.  Passau
21.  Ismaning
